# Create Cloudwatch event to execute the application every day at
resource "aws_cloudwatch_event_rule" "everyday_rule" {
  name                = "start_crawler_trigger"
  description         = "schedule events for crawler"
  depends_on = ["aws_lambda_function.putSeedInQueue"]
  schedule_expression = "cron(0 14 * * ? *)"
  tags ={
    Application="iw-bd-demowebcrawler"
  }
}

# Target the event created to putSeedsFunction
resource "aws_cloudwatch_event_target" "start_crawler" {
  rule =  "${aws_cloudwatch_event_rule.everyday_rule.name}"
  arn  =   "${aws_lambda_function.putSeedInQueue.arn}"
  target_id = "putSeedInQueue"
}

resource "aws_lambda_permission" "start_crawler_per" {
  statement_id = "AllowExecutionFromCloudWatch"
  action = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.putSeedInQueue.function_name}"
  principal = "events.amazonaws.com"
  source_arn = "${aws_cloudwatch_event_rule.everyday_rule.arn}"
}
# event source mapping between SQS and Lambda queue -> WebsiteWatcher
resource "aws_lambda_event_source_mapping" "endpoint_seeds_queue" {
  batch_size        = 1
  event_source_arn  = "${aws_sqs_queue.seeds_v1.arn}"
  enabled           = true
  function_name     = "${aws_lambda_function.websiteWatcher.function_name}"
}

# event source mapping between SQS Lambda cityURLQueue -> hyperlinkCollector
resource "aws_lambda_event_source_mapping" "endpoint_cityURLQueue" {
  batch_size        = 1
  event_source_arn  = "${aws_sqs_queue.cityURLqueue_v1.arn}"
  enabled           = true
  function_name     = "${aws_lambda_function.hyperlinkCollector.function_name}"
}

# event source mapping between SQS Lambda pdfURLQueue -> hyperlinkProcessor
resource "aws_lambda_event_source_mapping" "endpoint_pdfURLQueue" {
  batch_size        = 1
  event_source_arn  = "${aws_sqs_queue.pdfURLqueue.arn}"
  enabled           = true
  function_name     = "${aws_lambda_function.hyperlinkProcessor.function_name}"
}
