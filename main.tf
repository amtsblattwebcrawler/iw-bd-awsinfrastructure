provider "aws" {
  region     = "eu-central-1"
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
}

# main bucket for Terraform state
//resource "aws_s3_bucket" "iw-bd-demowebcrawler-state" {
//    bucket = "iw-bd-demowebcrawler-state"
//    acl    = "private"
//}

# main bucket for Terraform state
resource "aws_s3_bucket" "iw-bd-demowebcrawler-pdf" {
  bucket = "iw-bd-demowebcrawler-pdf"
  acl    = "private"
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}

# main bucket for Lambda function
resource "aws_s3_bucket" "iw-bd-demowebcrawler-lambda" {
  bucket = "iw-bd-demowebcrawler-lambda"
  acl    = "private"
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}
# Define the remote repository for the .tfstate file

terraform {
  backend "s3" {
    bucket = "iw-bd-demowebcrawler-state"
    key    = "tfstate/terraform.tfstate.json"
    region = "eu-central-1"
  }
}

data "aws_s3_bucket_object" "testlambdapushS3_sourcehash" {
  bucket = "iw-bd-demowebcrawler-lambda"
  key    = "testlambdapushS3/testlambdapushS3.zip"
}

resource "aws_lambda_function" "iw-bigdata-test2" {
  function_name    = "iw-bigdata-testlambdapushS3"
  s3_bucket = "iw-bd-demowebcrawler-lambda"
  s3_key = "testlambdapushS3/testlambdapushS3.zip"
  role             = "arn:aws:iam::419206837402:role/service-role/MyLambdaExecutionRole"
  handler          = "main.lambda_handler"
  runtime          = "python3.6"
  source_code_hash = "${base64sha256(data.aws_s3_bucket_object.testlambdapushS3_sourcehash.last_modified)}"
}
