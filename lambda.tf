data "aws_s3_bucket_object" "putSeedInQueue_sourcehash" {
  bucket = "iw-bd-demowebcrawler-lambda"
  key    = "putSeedInQueue/putSeedInQueue.zip"
}

resource "aws_lambda_function" "putSeedInQueue" {
  function_name    = "putSeedInQueue_v1"
  role             = "${aws_iam_role.example_lambda.arn}"
  handler          = "main.main"
  runtime          = "python3.6"
  s3_bucket = "iw-bd-demowebcrawler-lambda"
  s3_key = "putSeedInQueue/putSeedInQueue.zip"
  source_code_hash = "${base64sha256(data.aws_s3_bucket_object.putSeedInQueue_sourcehash.last_modified)}"
  timeout = 120
  memory_size = 128
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}

data "aws_s3_bucket_object" "websiteWatcher_sourcehash" {
  bucket = "iw-bd-demowebcrawler-lambda"
  key    = "websiteWatcher/websiteWatcher.zip"
}

resource "aws_lambda_function" "websiteWatcher" {
  function_name    = "websiteWatcher_v1"
  role             = "${aws_iam_role.example_lambda.arn}"
  handler          = "main.lambda_handler"
  runtime          = "python3.6"
  s3_bucket = "iw-bd-demowebcrawler-lambda"
  s3_key= "websiteWatcher/websiteWatcher.zip"
  source_code_hash = "${base64sha256(data.aws_s3_bucket_object.websiteWatcher_sourcehash.last_modified)}"
  timeout = 120
  memory_size = 128
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}

data "aws_s3_bucket_object" "hyperlinkCollector_sourcehash" {
  bucket = "iw-bd-demowebcrawler-lambda"
  key    = "hyperlinkCollector/hyperlinkCollector.zip"
}

resource "aws_lambda_function" "hyperlinkCollector" {
  function_name    = "hyperlinkCollector_v1"
  role             = "${aws_iam_role.example_lambda.arn}"
  handler          = "hyperlinkCollector.main"
  runtime          = "python3.6"
  s3_bucket = "iw-bd-demowebcrawler-lambda"
  s3_key= "hyperlinkCollector/hyperlinkCollector.zip"
  source_code_hash = "${base64sha256(data.aws_s3_bucket_object.hyperlinkCollector_sourcehash.last_modified)}"
  timeout = 300
  memory_size = 128
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}

data "aws_s3_bucket_object" "hyperlinkProcessor_sourcehash" {
  bucket = "iw-bd-demowebcrawler-lambda"
  key    = "hyperlinkProcessor/hyperlinkProcessor.zip"
}

resource "aws_lambda_function" "hyperlinkProcessor" {
  function_name    = "hyperlinkProcessor_v1"
  role             = "${aws_iam_role.example_lambda.arn}"
  handler          = "hyperlinkProcessor.main"
  runtime          = "python3.6"
  s3_bucket = "iw-bd-demowebcrawler-lambda"
  s3_key= "hyperlinkProcessor/hyperlinkProcessor.zip"
  source_code_hash = "${base64sha256(data.aws_s3_bucket_object.hyperlinkProcessor_sourcehash.last_modified)}"
  timeout = 120
  memory_size = 128
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}