resource "aws_sqs_queue" "seeds_v1" {
    name                       = "seeds_v1"
    tags = {
      Application="iw-bd-demowebcrawler"
    }
    visibility_timeout_seconds = 150
    message_retention_seconds  = 345600
    max_message_size           = 262144
    delay_seconds              = 0
    receive_wait_time_seconds  = 0
    redrive_policy             = <<POLICY
    {
      "deadLetterTargetArn": "${aws_sqs_queue.seeds-deadletter.arn}",
      "maxReceiveCount": 5
    }
POLICY
}

resource "aws_sqs_queue" "seeds-deadletter" {
  name = "seeds_deadletter"
  max_message_size = 262144 #256kb
  message_retention_seconds = 1209600 #14 days
  visibility_timeout_seconds =90
  receive_wait_time_seconds = 20
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}

resource "aws_sqs_queue" "cityURLqueue_v1" {
    name                       = "cityURLqueue_v1"
    tags = {
      Application="iw-bd-demowebcrawler"
    }
    visibility_timeout_seconds = 180
    message_retention_seconds  = 345600
    max_message_size           = 262144
    delay_seconds              = 0
    receive_wait_time_seconds  = 0
    redrive_policy             =  <<POLICY
    {
      "deadLetterTargetArn": "${aws_sqs_queue.cityURLqueue-deadletter.arn}",
      "maxReceiveCount": 3
    }
POLICY
}

resource "aws_sqs_queue" "cityURLqueue-deadletter" {
  name = "cityURLqueue_deadletter"
  max_message_size = 262144 #256kb
  message_retention_seconds = 1209600 #14 days
  visibility_timeout_seconds = 300
  receive_wait_time_seconds = 20
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}

resource "aws_sqs_queue" "pdfURLqueue" {
    name                       = "pdfURLqueue_v1"
    tags = {
      Application="iw-bd-demowebcrawler"
    }
    visibility_timeout_seconds = 150
    message_retention_seconds  = 345600
    max_message_size           = 262144
    delay_seconds              = 0
    receive_wait_time_seconds  = 0
    redrive_policy             =  <<POLICY
      {
        "deadLetterTargetArn": "${aws_sqs_queue.pdfURLqueue-deadletter.arn}",
        "maxReceiveCount": 200
      }
  POLICY
}

resource "aws_sqs_queue" "pdfURLqueue-deadletter" {
  name = "pdfURLqueue_deadletter"
  max_message_size = 262144 #256kb
  message_retention_seconds = 1209600 #14 days
  visibility_timeout_seconds = 300
  receive_wait_time_seconds = 20
  tags = {
    Application="iw-bd-demowebcrawler"
  }
}





